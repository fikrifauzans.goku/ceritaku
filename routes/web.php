<?php

use App\Http\Controllers\StoryController;
use Illuminate\Support\Facades\Route;
use App\Models\Story;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                                  [StoryController::class, 'index']);
Route::get('/stories',                           [StoryController::class, 'show']);
Route::get('/stories/{slug:slug}',               [StoryController::class, 'post']);
Route::get('/stories/category/{category:slug}',  [StoryController::class, 'categoryPost']);
Route::get('/author/{user:username}',            [StoryController::class, 'showPost']);
Route::get('/categories',                        [CategoryController::class, 'show']);

