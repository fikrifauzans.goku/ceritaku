<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Story;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        // User::create([
            //     'name' => 'Fikri Fauzan',
            //     'email' => 'fik@gmail.com',
            //     'password' => bcrypt(123456)
            // ]);
            // User::create([
                //     'name' => 'Rizal Nugraha',
                //     'email' => 'faddill@gmail.com',
                //     'password' => bcrypt(123456)
                // ]);
                // User::create([
                    //     'name' => 'Fadillah Hakim',
                    //     'email' => 'Rzl@gmail.com',
        //     'password' => bcrypt(123456)
        // ]);
        // User::create([
        //     'name' => 'Anggi Geovani',
        //     'email' => 'Aang@gmail.com',
        //     'password' => bcrypt(123456)
        // ]);

        User::factory(5)->create();

        Category::create([
            'name' => 'Romance Story',
            'image' => 'romace.jpg',
            'slug' => 'romance-story',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolore, exercitationem.'


        ]);
        Category::create([
            'name' => 'Horor Story',
            'image' => 'horor.jpg',
            'slug' => 'horor-story',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolore, exercitationem.'
        ]);
        Category::create([
            'name' => 'Funny Story',
            'image' => 'funny.jpg',
            'slug' => 'funny-story',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolore, exercitationem.'
        ]);
        Category::create([
            'name' => 'Sad Story',
            'image' => 'sad.jpg',
            'slug' => 'sad-story',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolore, exercitationem.'
        ]);


        // Story::factory(20)->create();
        Story::factory(25)->create();
        // Story::create([
        //     'category_id' => 1,
        //     'user_id' => 1,
        //     'title' => 'Mencarimu',
        //     'slug' => 'mencari-mu',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placphpeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?']);

        // Story::create([
        //     'category_id' => 2,
        //     'user_id' => 1,
        //     'title' => 'Biarkan Dia Mati',
        //     'slug' => 'biarkan-dia-mati',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        // ]);
        // Story::create([
        //     'category_id' => 2,
        //     'user_id' => 2,
        //     'title' => 'Cari Aku Dalam Kegelapan',
        //     'slug' => 'cari-aku-dalam-kegelapan',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        // ]);
        // Story::create([
        //     'category_id' => 3,
        //     'user_id' => 4,
        //     'title' => 'Lucu Ala Bapack',
        //     'slug' => 'cari-aku-dalam-kegelapan',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        // ]);
        // Story::create([
        //     'category_id' => 3,
        //     'user_id' => 4,
        //     'title' => 'Jangan tinggalkan Aku',
        //     'slug' => 'jangan-tinggalkan-aku',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        // ]);
        // Story::create([
        //     'category_id' => 4,
        //     'user_id' => 4,
        //     'title' => 'Menangislah Selagi Kau Bisa',
        //     'slug' => 'mengangislah-selagi-kau-bisa',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        // ]);
        // Story::create([
        //     'category_id' => 1,
        //     'user_id' => 2,
        //     'title' => 'Aku Cinta',
        //     'slug' => 'aku cinta',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        // ]);
    }
}
