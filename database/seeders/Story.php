<?php


namespace Database\Seeders;

use GuzzleHttp\Promise\Create;
use Illuminate\Database\Seeder;
use App\Models\Story;



class Stories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Story::create([
            'category_id' => 1,
            'user_id' => 1,
            'title' => 'Mencarimu',
            'slug' => 'mencari-mu',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?']);

        Story::create([
            'category_id' => 2,
            'user_id' => 1,
            'title' => 'Biarkan Dia Mati',
            'slug' => 'biarkan-dia-mati',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        ]);
        Story::create([
            'category_id' => 2,
            'user_id' => 2,
            'title' => 'Cari Aku Dalam Kegelapan',
            'slug' => 'cari-aku-dalam-kegelapan',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        ]);
        Story::create([
            'category_id' => 3,
            'user_id' => 4,
            'title' => 'Lucu Ala Bapack',
            'slug' => 'cari-aku-dalam-kegelapan',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        ]);
        Story::create([
            'category_id' => 3,
            'user_id' => 4,
            'title' => 'Jangan tinggalkan Aku',
            'slug' => 'jangan-tinggalkan-aku',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        ]);
        Story::create([
            'category_id' => 4,
            'user_id' => 4,
            'title' => 'Menangislah Selagi Kau Bisa',
            'slug' => 'mengangislah-selagi-kau-bisa',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        ]);
        Story::create([
            'category_id' => 1,
            'user_id' => 2,
            'title' => 'Aku Cinta',
            'slug' => 'aku cinta',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, itaque. Necessitatibus labore quae qui, molestias nemo, magni ipsum quasi voluptatibus, nisi laudantium cum ab cumque ad fugit? Quidem, asperiores! Nesciunt earum est possimus eum eveniet commodi velit nulla adipisci accusantium, praesentium cupiditate alias similique totam eaque dignissimos sit doloribus temporibus maxime explicabo perspiciatis. Consequuntur aspernatur officia culpa beatae voluptates quasi doloribus iure repellat eveniet totam porro vel qui libero id enim est dicta atque velit, quo optio obcaecati? Sit officiis inventore a nemo quas velit aspernatur necessitatibus provident illum, dolorum rerum laudantium qui ipsam culpa voluptatibus error voluptatem quibusdam nisi?'
        ]);




    }




}
