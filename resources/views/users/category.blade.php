@extends('layouts.master')

@section('contain')
<h1>{{ $title }}</h1>

@foreach ($categories->stories as $category)
<h3 class="mt-5"><a href="stories">{{ $category->title }}</a></h3>
<p><a href="">{{ $category->user->name }}</a></p>
<strong>{{ $category->author }}</strong>
<p>{{ $category->body }}</p>
@endforeach

@endsection
