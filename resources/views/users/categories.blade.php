@extends('layouts.master')


@section('contain')

<h1 class='text-center'>Kategori Cerita</h1>
<div class="d-flex mt-3 ">
@foreach ($user as $category)
<div class='card-group '>

<div class="card bg-light mb-2 mr-3" style="max-width: 18rem;">

    <div class="card-body">
        <img class="card-img-top mb-3" src="img/categories/{{ $category->image }}" alt="Card image cap">
      <h5 class="card-title">{{ $category->name }}</h5>
      <p class="card-text">{{  $category->description }}</p>
      <a href='/stories/category/{{ $category->slug }}'  type="button" class="btn btn-outline-success">Litah Cerita >></a>
    </div>
</div>
</div>
@endforeach
</div>

@endsection
