
@extends('layouts.master')
@section('contain')
<h1 class='text-center'>{{ $header }}</h1>
    @foreach ($stories as $story)
        <h3 class="mt-5"><a href="/stories/{{ $story->slug }}">{{ $story->title }}</a></h3>
        <strong><a href="/author/{{ $story->user->username }}">{{ $story->user->name }}</a></strong> In <a href="/stories/category/{{ $story->category->slug }}">{{ $story->category->name }}</a>
        <p class="border-bottom pb-4">{{ $story->body }}</p>

    @endforeach
@endsection






