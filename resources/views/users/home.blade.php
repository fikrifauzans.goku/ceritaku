@extends('layouts.master')

@section('title')
    Home
@endsection

@section('header')
@endsection

@section('contain')
<div class="card bg-dark text-white mb-4">
    <img class="card-img" src="{{ asset('img/home/home.jpg') }}" alt="Card image" width="100%" height="600px">

    <style>
        img{
            object-fit: cover;
        }
    </style>
  </div>

    <strong>Ceritaku</strong>

    <p >Ceritaku adalah kumpulan cerita dan pengalaman yang di tulis oleh penulis dalam dunia nyata. ceritaku di buat untuk mengungkapkan isi hati penulis melalui sebuah blog sederhana. tujuan di buatnya blog sederhana ini agar penulis dapat berbagi pengalaman juga meminta saran dan masukan untuk para pembacanya. </p>
    <style>
     p { font-size: 20px;
        margin-top: 10px;

        }
     strong { font-size: 25px}


    </style>
@endsection
