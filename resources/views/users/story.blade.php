@extends('layouts.master')
@section('contain')
<h3 class="mt-5">{{ $story->title }}</h3>
<strong>{{ $story->user->name }}</strong>
<p>{{ $story->body }}</p>
<a href="/stories"><< back</a>

@endsection
