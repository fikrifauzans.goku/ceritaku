<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'image',
        'slug'
    ];
    /**
     * Get the user that owns the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
    * Get all of the comments for the Category
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function stories(){

    return $this->hasMany(Story::class, 'category_id');

    }
}

