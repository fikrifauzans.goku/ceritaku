<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Story;
use App\Models\Category;
use App\Models\User;

class StoryController extends Controller
{
    public function index(){
        return view('users.home', ["title" => "home"]);
    }

    public function show() {

        return view('users.stories', [
            'stories' => Story::with(['user','category'])->latest()->get(),
            'title' => 'stories',
            'header' => 'All Stories'
        ]);

    }

    public function categoryPost(Category $category){

        return view('users.stories',[
            'stories' => $category->stories->load(['category','user']),
            'title' => 'stories',
            'header' => $category->name
        ]);
    }

    public function showPost(User $user){


        return view('users.stories',[
            'stories' => $user->stories->load(['user' , 'category']),
            'title' => 'stories',
            'header' => 'Writed By: '.$user->name
        ]);
    }

    public function post(Story $slug){

        return view('users.story', [
            "story" => $slug,
            "title" => 'story'
        ]);

        }


}
