<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Story;
class CategoryController extends Controller
{
        public function show(){
            return view('users.categories' ,[
                'user' => Category::all(),
                'title' => 'categories'
            ], );
        }

    }
